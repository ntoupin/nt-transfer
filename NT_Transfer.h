/*
 * @file NT_Transfer_t.h
 * @author Nicolas Toupin
 * @date 2020-03-30
 * @brief Transfer function declarations.
 */

#ifndef NT_Controller_h

#include "Arduino.h"
#include "NT_Control.h"

#define TRUE 1
#define FALSE 0

#define NT_Transfer_t_h
#define Ref(Struct) (int*)&Struct

#define START_BYTE 0x06
#define SYNC_BYTE 0x85

class Transfer_t
{
public:
	Transfer_t();
	void Begin(int* Structure_Ptr, Stream* Stream);
	void Send();
	bool Receive();

private:
	int* _Address = 0;
	byte _Size = 0;
	Stream* _Stream = 0;
	int* _RxBuffer = 0;
	byte _RxLength = 0;
	byte _RxArrayX = 0;
	byte _Checksum = 0;
	byte _ChecksumCalculated = 0;
};

#endif
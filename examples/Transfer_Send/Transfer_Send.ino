/*
@file Transfer.ino
@author Nicolas Toupin
@date 2020 - 03 - 20
@brief Simple example of controller utilisation.
*/

#include <NT_Transfer.h>

struct Data_t {
  bool Led;
};

Data_t Data1;
Transfer_t Transfer1;

void setup()
{
  Serial.begin(9600);
  Transfer1.Begin(Ref(Data1), &Serial);

  // Init led status
  Data1.Led = FALSE;
}

void loop()
{
  // Toogle led every loop iteration
  if (Data1.Led == FALSE)
  {
    Data1.Led = TRUE;
  }
  else
  {
    Data1.Led = FALSE;
  }

  // Send data to serial port
  Transfer1.Send();

  // Send delay sould be longer than receive delay
  delay(1000);
}

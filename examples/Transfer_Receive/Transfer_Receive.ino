/*
   @file Transfer.ino
   @author Nicolas Toupin
   @date 2020-03-20
   @brief Simple example of controller utilisation.
*/

#include <NT_Transfer.h>

struct Data_t {
  bool Led;
};

Data_t Data1;
Transfer_t Transfer1;

void setup()
{
  Serial.begin(9600);
  Transfer1.Begin(Ref(Data1), &Serial);

  // Init led status
  Data1.Led = FALSE;
}

void loop()
{
  // Receive data from serial port
  Transfer1.Receive();

  // Write received led status
  digitalWrite(LED_BUILTIN, Data1.Led);

  // Receive delay sould be sorter then send delay
  delay(10);
}

/*
 * @file NT_Transfer_t.cpp
 * @author Nicolas Toupin
 * @date 2020-03-30
 * @brief Transfer function definitions.
 */

#include "Arduino.h"
#include "NT_Transfer.h"

Transfer_t::Transfer_t()
{
}

void Transfer_t::Begin(int* Structure_Ptr, Stream* Stream)
{
	// Set information about the communication channel
	_Address = Structure_Ptr;
	_Size = sizeof(Structure_Ptr);
	//_Size = Size;
	_Stream = Stream;

	// Dynamic creation of rx parsing buffer in RAM
	_RxBuffer = (int*)malloc(_Size + 1);
}

void Transfer_t::Send()
{
	// Send syncronisation bytes
	_Stream->write(START_BYTE);
	_Stream->write(SYNC_BYTE);

	// Send data size
	_Stream->write(_Size);

	// Send data and compute checksum
	byte _Checksum = _Size;
	for (int i = 0; i < _Size; i++) {
		_Checksum ^= *(_Address + i);
		_Stream->write(*(_Address + i));
	}

	// Send checksum
	_Stream->write(_Checksum);
}

bool Transfer_t::Receive()
{
	// Wait for the header bytes. If they were already found in a previous call, skip it.
	if (_RxLength == 0)
	{
		// Wait until there is 3 bytes into queue
		if (_Stream->available() >= 3)
		{
			// Wait for start byte
			while (_Stream->read() != START_BYTE)
			{
				// Remove stuff before start byte
				if (_Stream->available() < 3)
					return FALSE;
			}

			if (_Stream->read() == SYNC_BYTE)
			{
				// Read size of receive structure
				_RxLength = _Stream->read();

				//Compare received structure size to structure on this controller
				if (_RxLength != _Size) {
					_RxLength = 0;
					return FALSE;
				}
			}
		}
	}

	// Start byte, sync byte and size are read, now read the data
	if (_RxLength != 0)
	{
		// Read data
		while (_Stream->available() && _RxArrayX <= _RxLength)
		{
			_RxBuffer[_RxArrayX++] = _Stream->read();
		}

		// Whole message has bee read
		if (_RxLength == (_RxArrayX - 1))
		{
			// Compute checksum of received data
			_ChecksumCalculated = _RxLength;
			for (int i = 0; i < _RxLength; i++)
			{
				_ChecksumCalculated ^= _RxBuffer[i];
			}

			// Compare checksum
			if (_ChecksumCalculated == _RxBuffer[_RxArrayX - 1])
			{
				// Checksum is good
				memcpy(_Address, _RxBuffer, _Size);
				_RxLength = 0;
				_RxArrayX = 0;
				return TRUE;
			}
			else
			{
				// Cheecksum is bad
				_RxLength = 0;
				_RxArrayX = 0;
				return FALSE;
			}
		}
	}
	return FALSE;
}